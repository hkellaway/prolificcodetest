//
//  AppDelegate.swift
//  ProlificCodeTest
//
//  Created by Harlan Kellaway on 1/17/19.
//  Copyright © 2019 Harlan Kellaway. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    // MARK: - Properties

    var window: UIWindow?
    var currentAPIEnvironment: APIEnvironment = .undefined
    var isAccessibilityOn: Bool = false
    
    // MARK: Private properties

    private var dependencyManager: DependencyManager!
    
    // MARK: - Protocol conformance
    
    // MARK: UIApplicationDelegate
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        configureApplicationContext()
        registerForAccessibilityNotifications()
        
        assert(!currentAPIEnvironment.isUndefined)
        
        return dependencyManager.router().installRootView()
    }
    
    // MARK: - Instance functions
    
    // MARK: Private instance functions
    
    private func configureApplicationContext()  {
        // This should be the only point in the codebase where the build configuration is checked;
        // all dependencies should be created and configured based on Environment thereafter
        #if RELEASE
        currentAPIEnvironment = .production
        #else
        currentAPIEnvironment = .staging
        #endif
        isAccessibilityOn = UIAccessibility.isVoiceOverRunning
        DependencyManager.shared.apiEnvironment = currentAPIEnvironment
        DependencyManager.shared.isAccessibilityOn = isAccessibilityOn
        self.dependencyManager = DependencyManager.shared
    }
    
    private func registerForAccessibilityNotifications() {
        dependencyManager.notificationCenter().addObserver(self,
                                                           selector: #selector(accessibilityStatusDidChange),
                                                           name: UIAccessibility.voiceOverStatusDidChangeNotification,
                                                           object: nil)
    }
    
    @objc private func accessibilityStatusDidChange(_ notification: Notification) {
        let statusChanged = isAccessibilityOn != UIAccessibility.isVoiceOverRunning
        if !statusChanged {
            return
        }
        switchTo(newAccessibilityStatus: UIAccessibility.isVoiceOverRunning)
    }

}

// MARK: - Protocol conformance

// MARK: ApplicationContextSwitcher

extension AppDelegate: ApplicationContextSwitcher {

    func switchTo(newAPIEnvironment: APIEnvironment) {
        currentAPIEnvironment = newAPIEnvironment
        restart()
    }

    func switchTo(newAccessibilityStatus isAccessibilityOn: Bool) {
        self.isAccessibilityOn = isAccessibilityOn
        restart()
    }

    func restart() {
        dependencyManager.restart(apiEnvironment: currentAPIEnvironment,
                                  isAccessibilityOn: isAccessibilityOn,
                                  rootWindow: window)
    }
    
    func toggleHighLatency() {
        guard !currentAPIEnvironment.isProduction else {
            return
        }
        
        dependencyManager.toggleHighLatency()
    }

}
