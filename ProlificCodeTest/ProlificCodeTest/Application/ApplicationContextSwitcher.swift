//
//  ApplicationContextSwitcher.swift
//  ProlificCodeTest
//
//  Created by Harlan Kellaway on 1/17/19.
//  Copyright © 2019 Harlan Kellaway. All rights reserved.
//

import Foundation

/// Owns properties that change the context the application is operating in.
protocol ApplicationContextSwitcher {
    
    /// Whether Accessibility is currently on.
    var isAccessibilityOn: Bool { get }
    
    /// Current Environment.
    var currentAPIEnvironment: APIEnvironment { get }
    
    /// Switches application context to new Accessibility status.
    ///
    /// - Parameter isAccessibilityOn: Whether Accessbility was turned on.
    func switchTo(newAccessibilityStatus isAccessibilityOn: Bool)
    
    /// Switches application context to use new API Environment.
    ///
    /// - Parameter newAPIEnvironment: New API Environment.
    func switchTo(newAPIEnvironment: APIEnvironment)
    
    /// Restarts the application using current context.
    func restart()
    
    /// Toggles whether application is simulating High Latency.
    func toggleHighLatency()
    
}
