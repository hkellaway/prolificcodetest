//
//  BookDetailViewController.swift
//  ProlificCodeTest
//
//  Created by Harlan Kellaway on 1/18/19.
//  Copyright © 2019 Harlan Kellaway. All rights reserved.
//

import UIKit

class BookDetailViewController: UIViewController {
    
    // MARK: - Properties
    
    /// Book.
    var book: BookDisplayModel!
    
    // MARK: - Overrides
    
    // MARK: View life-cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupDesign()
    }
    
    // MARK: - Instance functions
    
    // MARK: Private instance functions
    
    private func setupDesign() {
        title = book.title
    }
    
}
