//
//  RootTabBarController.swift
//  ProlificCodeTest
//
//  Created by Harlan Kellaway on 1/17/19.
//  Copyright © 2019 Harlan Kellaway. All rights reserved.
//

import UIKit

/// Root tab bar of application.
class RootTabBarController: UITabBarController {
    
    // MARK: - Types
    
    enum Tab: Int {
        case home
        case myLibrary
    }
    
    // MARK: - Properties
    
    /// Feature builder.
    var featureBuilder: FeatureBuilder!
    
    // MARK: - Instance functions
    
    /// Currently selected tab.
    ///
    /// - Returns: Currently selected tab.
    func selectedTab() -> Tab {
        return Tab(rawValue: selectedIndex)!
    }
    
    /// Selects provided tab.
    ///
    /// - Parameter tab: Tab to select.
    func selectTab(_ tab: Tab) {
        selectedIndex = tab.rawValue
    }
    
}
