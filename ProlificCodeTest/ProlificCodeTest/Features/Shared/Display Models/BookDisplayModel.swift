//
//  BookDisplayModel.swift
//  ProlificCodeTest
//
//  Created by Harlan Kellaway on 1/18/19.
//  Copyright © 2019 Harlan Kellaway. All rights reserved.
//

import Foundation

struct BookDisplayModel {
    
    // MARK: - Properties
    
    /// Title.
    let title: String
    
    // MARK: - Initialization
    
    init(book: Book) {
        self.init(title: book.title)
    }
    
    init(title: String) {
        self.title = title
    }
    
}
