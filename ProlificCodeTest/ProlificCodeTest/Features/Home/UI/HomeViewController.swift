//
//  HomeViewController.swift
//  ProlificCodeTest
//
//  Created by Harlan Kellaway on 1/17/19.
//  Copyright © 2019 Harlan Kellaway. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController, HomeView {
    
    // MARK: - Properties
    
    /// View model.
    var viewModel: HomeViewModel!
    
    // MARK: Private properties
    
    private var books: [BookDisplayModel] = []
    
    private var loadingIndicator: UIActivityIndicatorView?
    private var refreshControl: UIRefreshControl?
    @IBOutlet private var tableView: UITableView!
    
    // MARK: - Overrides
    
    // MARK: View life-cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView?.dataSource = self
        tableView?.delegate = self
        
        setupDesign()
        viewModel.loadBooks()
    }
    
    // MARK: - Protocol conformance
    
    // MARK: HomeView
    
    func displayBooks(_ books: [BookDisplayModel]) {
        self.books = books
        tableView?.reloadData()
        refreshControl?.endRefreshing()
    }
    
    func displayAsLoading() {
        loadingIndicator?.isHidden = false
        loadingIndicator?.startAnimating()
    }
    
    func clearLoadingDisplay() {
        loadingIndicator?.stopAnimating()
        loadingIndicator?.isHidden = true
    }
    
    func displayError(_ error: String) {
        print("FAILURE: \(error)")
    }
    
    // MARK: - Instance functions
    
    // MARK: Private instance functions
    
    private func setupDesign() {
        navigationItem.title = "Welcome" // TODO: Localize
        setupLoadingIndicator()
        setupRefresh()
        loadingIndicator?.isHidden = true
    }
    
    private func setupLoadingIndicator() {
        let loadingIndicator = UIActivityIndicatorView()
        self.loadingIndicator = loadingIndicator
        loadingIndicator.color = .black
        view.addSubview(loadingIndicator)
        view.bringSubviewToFront(loadingIndicator)
        loadingIndicator.center = view.center
    }
    
    private func setupRefresh() {
        let refreshControl = UIRefreshControl()
        self.refreshControl = refreshControl
        refreshControl.addTarget(self,
                                 action: #selector(refreshControlDidReceivePull),
                                 for: .valueChanged)
        tableView?.addSubview(refreshControl)
    }
    
    @objc private func refreshControlDidReceivePull() {
        viewModel.didSelectRefresh()
    }
    
}

// MARK: - Protocol conformance

// MARK: UITableViewDataSource

extension HomeViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return books.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let book = books[indexPath.row]
        let cell = UITableViewCell()
        cell.textLabel?.text = book.title
        return cell
    }
    
}

// MARK: - UITableViewDelegate

extension HomeViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let book = books[indexPath.row]
        viewModel.didSelectBook(book)
    }
    
}
