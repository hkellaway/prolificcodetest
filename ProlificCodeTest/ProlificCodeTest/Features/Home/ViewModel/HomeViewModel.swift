//
//  HomeViewModel.swift
//  ProlificCodeTest
//
//  Created by Harlan Kellaway on 1/17/19.
//  Copyright © 2019 Harlan Kellaway. All rights reserved.
//

import Foundation

final class HomeViewModel {
    
    // MARK: - Properties
    
    /// Home view.
    weak var view: HomeView?
    
    /// Repository.
    let repository: HomeRepository
    
    /// Router.
    let router: Router
    
    // MARK: - Initialization
    
    init(repository: HomeRepository, router: Router) {
        self.repository = repository
        self.router = router
    }
    
    // MARK: - Instance functions
    
    /// Loads Books.
    func loadBooks() {
        view?.displayAsLoading()
        repository.getBooks() { [weak self] result in
            self?.view?.clearLoadingDisplay()
            switch result {
            case .success(let books):
                self?.view?.displayBooks(books.map { BookDisplayModel(book: $0) })
            case .failure(let error):
                switch error {
                case HTTPClientError.responseInvalidFormat:
                    self?.view?.displayError("Whoopsie!")
                default:
                    self?.view?.displayError("Oops!")
                }
            }
        }
    }
    
    /// Indicates Reresh was selected.
    func didSelectRefresh() {
        loadBooks()
    }
    
    /// Indicates a Book was selected.
    ///
    /// - Parameter book: Selected Book.
    func didSelectBook(_ book: BookDisplayModel) {
        _ = router.routeToBookDetail(book)
    }
    
}
