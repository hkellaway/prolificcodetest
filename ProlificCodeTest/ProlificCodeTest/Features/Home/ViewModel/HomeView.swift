//
//  HomeView.swift
//  ProlificCodeTest
//
//  Created by Harlan Kellaway on 1/17/19.
//  Copyright © 2019 Harlan Kellaway. All rights reserved.
//

import Foundation

protocol HomeView: class {
    
    func displayBooks(_ books: [BookDisplayModel])
    func displayAsLoading()
    func clearLoadingDisplay()
    func displayError(_ error: String)
    
}
