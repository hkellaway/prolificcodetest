//
//  HomeRepository.swift
//  ProlificCodeTest
//
//  Created by Harlan Kellaway on 1/17/19.
//  Copyright © 2019 Harlan Kellaway. All rights reserved.
//

import Foundation

protocol HomeRepository {
    
    func getBooks(completion: @escaping (Result<[Book]>) -> Void)
    
}
