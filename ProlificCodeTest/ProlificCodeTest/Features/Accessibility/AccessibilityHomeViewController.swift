//
//  AccessibilityHomeViewController.swift
//  ProlificCodeTest
//
//  Created by Harlan Kellaway on 1/17/19.
//  Copyright © 2019 Harlan Kellaway. All rights reserved.
//

import UIKit

final class AccessibilityHomeViewController: HomeViewController {
    
    // MARK: - Properties
    
    // MARK: Private properties
    
    private var allyBooks: [BookDisplayModel] = []
    
    @IBOutlet private var allyTableView: UITableView!
    @IBOutlet private var allyRefreshButton: UIBarButtonItem!
    
    // MARK: Overrides
    
    // MARK: View life-cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        allyTableView.dataSource = self
        allyTableView.delegate = self
        
        setupDesign()
    }
    
    // MARK: HomeViewController
    
    override func displayBooks(_ books: [BookDisplayModel]) {
        self.allyBooks = books
        allyTableView.reloadData()
    }
    
    override func displayAsLoading() {
        let loadingIndicator = UINavigationController(rootViewController: AcessibilityLoadingIndicator())
        present(loadingIndicator, animated: true, completion: nil)
    }
    
    override func clearLoadingDisplay() {
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Instance functions
    
    // MARK: Private instance functions
    
    private func setupDesign() {
        navigationItem.title = "Welcome, VoiceOver user!"
        setupRefresh()
    }
    
    private func setupRefresh() {
        allyRefreshButton.target = self
        allyRefreshButton.action = #selector(refreshButtonDidSelect)
    }
    
    @objc private func refreshButtonDidSelect() {
        viewModel.didSelectRefresh()
    }
    
}

// MARK: - Protocol conformance

// MARK: UITableViewDataSource

extension AccessibilityHomeViewController {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allyBooks.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let book = allyBooks[indexPath.row]
        let cell = UITableViewCell()
        cell.textLabel?.text = book.title
        return cell
    }
    
}

// MARK: UITableViewDelegate

extension AccessibilityHomeViewController {
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let book = allyBooks[indexPath.row]
        viewModel.didSelectBook(book)
    }
    
}
