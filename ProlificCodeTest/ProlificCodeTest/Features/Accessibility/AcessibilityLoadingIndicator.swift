//
//  AcessibilityLoadingIndicator.swift
//  ProlificCodeTest
//
//  Created by Harlan Kellaway on 1/18/19.
//  Copyright © 2019 Harlan Kellaway. All rights reserved.
//

import UIKit

final class AcessibilityLoadingIndicator: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        navigationItem.title = "Loading"
    }
    
}
