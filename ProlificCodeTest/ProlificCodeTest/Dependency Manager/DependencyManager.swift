//
//  DependencyManager.swift
//  ProlificCodeTest
//
//  Created by Harlan Kellaway on 1/17/19.
//  Copyright © 2019 Harlan Kellaway. All rights reserved.
//

import UIKit

/// Maintains definitions and injection of application dependencies.
final class DependencyManager: FeatureBuilder {
    
    // MARK: - Class properties
    
    /// Shared instance.
    static var shared = DependencyManager()
    
    // MARK: - Instance properties
    
    /// API Environment.
    var apiEnvironment: APIEnvironment
    
    /// Accessibility status.
    var isAccessibilityOn: Bool
    
    /// Root window of application.
    var rootWindow: UIWindow? {
        didSet {
            router().rootWindow = rootWindow
        }
    }
    
    // MARK: Private properties
    
    private var _httpClient: BasicHTTPClient?
    private var _repository: Repository?
    
    init(apiEnvironment: APIEnvironment = .undefined,
         isAccessibilityOn: Bool = UIAccessibility.isVoiceOverRunning,
         rootWindow: UIWindow? = UIApplication.shared.delegate?.window as? UIWindow) {
        self.apiEnvironment = apiEnvironment
        self.isAccessibilityOn = isAccessibilityOn
        self.rootWindow = rootWindow
    }
    
    // MARK: - Instance functions
    
    func restart(apiEnvironment: APIEnvironment, isAccessibilityOn: Bool, rootWindow: UIWindow?) {
        self.apiEnvironment = apiEnvironment
        self.isAccessibilityOn = isAccessibilityOn
        self.rootWindow = rootWindow
        _httpClient = nil
        _repository = nil
    }
    
    func toggleHighLatency() {
        guard !apiEnvironment.isProduction else {
            assertionFailure("Should not be toggle High Latency in production")
            return
        }
        
        httpClient().toggleHighLatency()
    }
    
    // MARK: Dependency definitions
    
    func repository() -> Repository {
        if _repository == nil {
            _repository = Repository(httpClient: httpClient(),
                                     jsonParser: jsonParser())
        }
        
        return _repository!
    }
    
    func httpClient() -> HTTPClient {
        if _httpClient == nil {
            _httpClient = BasicHTTPClient(baseURL: apiEnvironment.baseURL(),
                                          timer: timer(),
                                          isSimulatingHighLatency: false)
        }
        
        return _httpClient!
    }
    
    func jsonParser() -> JSONParser {
        return JSONParser(jsonDecoder: JSONDecoder())
    }
    
    func timer() -> Timer {
        return Timer()
    }
    
    func notificationCenter() -> NotificationCenter {
        return NotificationCenter.default
    }
    
    func router() -> Router {
        Router.shared.featureBuilder = self
        return Router.shared
    }
    
    // MARK: FeatureBuilder
    
    func rootView() -> RootTabBarController {
        let viewController: RootTabBarController
        
        if isAccessibilityOn {
            viewController = UIStoryboard(name: "Accessibility", bundle: nil)
                .instantiateViewController(withIdentifier: "AccessibilityRootTabBarController")
                as! AccessibilityRootTabBarController
        } else {
            viewController = UIStoryboard(name: "Main", bundle: nil)
                .instantiateViewController(withIdentifier: "RootTabBarController") as! RootTabBarController
        }
        
        viewController.featureBuilder = self
        
        return viewController
    }
    
    func bookDetailView(book: BookDisplayModel) -> BookDetailViewController {
        let viewController: BookDetailViewController
        
        if isAccessibilityOn {
            viewController = UIStoryboard(name: "Accessibility", bundle: nil)
                .instantiateViewController(withIdentifier: "AccessibilityBookDetailViewController")
                as! AccessibilityBookDetailViewController
        } else {
            viewController = UIStoryboard(name: "Main", bundle: nil)
                .instantiateViewController(withIdentifier: "BookDetailViewController") as! BookDetailViewController
        }
        
        viewController.book = book
        
        return viewController
    }
    
    func homeView() -> HomeViewController {
        let viewController: HomeViewController
        
        if isAccessibilityOn {
            viewController = UIStoryboard(name: "Accessibility", bundle: nil)
                .instantiateViewController(withIdentifier: "AccessibilityHomeViewController")
                as! AccessibilityHomeViewController
        } else {
            viewController = UIStoryboard(name: "Main", bundle: nil)
                .instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        }
        
        let viewModel = HomeViewModel(repository: repository(),
                                      router: router())
        viewController.viewModel = viewModel
        viewModel.view = viewController
        return viewController
    }
    
    func myLibraryView() -> MyLibrarayViewController {
        let viewController: MyLibrarayViewController
        
        if isAccessibilityOn {
            viewController = UIStoryboard(name: "Accessibility", bundle: nil)
                .instantiateViewController(withIdentifier: "AccessibilityMyLibraryViewController")
                as! AccessibilityMyLibraryViewController
        } else {
            viewController = UIStoryboard(name: "Main", bundle: nil)
                .instantiateViewController(withIdentifier: "MyLibrarayViewController")
                as! MyLibrarayViewController
        }
        
        return viewController
    }
    
}
