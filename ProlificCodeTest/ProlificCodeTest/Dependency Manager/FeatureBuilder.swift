//
//  FeatureBuilder.swift
//  ProlificCodeTest
//
//  Created by Harlan Kellaway on 1/17/19.
//  Copyright © 2019 Harlan Kellaway. All rights reserved.
//

import Foundation

protocol FeatureBuilder: class {
    
    func rootView() -> RootTabBarController
    func bookDetailView(book: BookDisplayModel) -> BookDetailViewController
    func homeView() -> HomeViewController
    func myLibraryView() -> MyLibrarayViewController
    
}
