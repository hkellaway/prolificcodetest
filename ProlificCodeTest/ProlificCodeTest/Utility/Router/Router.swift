//
//  Router.swift
//  ProlificCodeTest
//
//  Created by Harlan Kellaway on 1/17/19.
//  Copyright © 2019 Harlan Kellaway. All rights reserved.
//

import UIKit

/// Handles all navigation for application.
final class Router {
    
    // MARK: - Class properties
    
    /// Shared instance.
    static var shared = Router()
    
    // MARK: - Properties
    
    /// Root window.
    var rootWindow: UIWindow?
    
    /// Feature builder.
    weak var featureBuilder: FeatureBuilder?
    
    // MARK: Private properties
    
    private var homeNavigationController: UINavigationController?
    private var myLibraryNavigationController: UINavigationController?
    
    // MARK: - Initialization
    
    init(rootWindow: UIWindow? =  UIApplication.shared.delegate?.window as? UIWindow,
         featureBuilder: FeatureBuilder? = nil) {
        self.rootWindow = rootWindow
        self.featureBuilder = featureBuilder
    }
    
    // MARK: - Instance functions
    
    @discardableResult
    /// Installs root view into root window of application.
    ///
    /// - Returns: True if successful, false otherwise.
    func installRootView() -> Bool {
        guard
            let rootTabBarController = featureBuilder?.rootView(),
            let homeView = featureBuilder?.homeView(),
            let myLibraryView = featureBuilder?.myLibraryView() else {
            assertionFailure("Should not have nil view.")
            return false
        }
        
        let homeNavigationController = UINavigationController(rootViewController: homeView)
        homeNavigationController.tabBarItem = UITabBarItem(title: "Home", image: nil, selectedImage: nil) // TODO: Localization
        self.homeNavigationController = homeNavigationController
        
        let myLibraryNavigationController = UINavigationController(rootViewController: myLibraryView)
        myLibraryNavigationController.tabBarItem = UITabBarItem(title: "My Library", image: nil, selectedImage: nil) // TODO: Localization
        self.myLibraryNavigationController = myLibraryNavigationController
        
        rootTabBarController.viewControllers = [UINavigationController(), UINavigationController()]
        rootTabBarController.viewControllers?[RootTabBarController.Tab.home.rawValue] = homeNavigationController
        rootTabBarController.viewControllers?[RootTabBarController.Tab.myLibrary.rawValue] = myLibraryNavigationController
        rootTabBarController.selectTab(.home)
        
        rootWindow?.rootViewController = rootTabBarController
        rootWindow?.makeKeyAndVisible()
        
        return true
    }
    
    /// Routes to Book Detail for provided Book.
    ///
    /// - Parameter book: Book.
    /// - Returns: TRue if routing successful, false otherwise.
    func routeToBookDetail(_ book: BookDisplayModel) -> Bool {
        guard let bookView = featureBuilder?.bookDetailView(book: book) else {
            assertionFailure("Should not have nil view.")
            return false
        }
        
        homeNavigationController?.pushViewController(bookView, animated: true)
        
        return true
    }
    
}
