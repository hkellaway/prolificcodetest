//
//  JSONParser.swift
//  ProlificCodeTest
//
//  Created by Harlan Kellaway on 1/17/19.
//  Copyright © 2019 Harlan Kellaway. All rights reserved.
//

import Foundation

/// Parses JSON into objects.
final class JSONParser {
    
    // MARK: - Properties
    
    /// JSON decoder.
    let jsonDecoder: JSONDecoder
    
    // MARK: - Inititialization
    
    init(jsonDecoder: JSONDecoder) {
        self.jsonDecoder = jsonDecoder
    }
    
    // MARK: - Instance functions
    
    /// Parses array of `Book`s from provided JSON.
    ///
    /// - Parameter json: JSON.
    /// - Returns: Books if successful, error otherwise.
    func parseBooks(_ data: Data) -> Result<[Book]> {
        do {
            let books = try jsonDecoder.decode([Book].self, from: data)
            return Result(value: books)
        } catch {
            return Result(error: error)
        }
    }
    
}
