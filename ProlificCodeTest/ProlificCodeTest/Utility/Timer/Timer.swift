//
//  Timer.swift
//  ProlificCodeTest
//
//  Created by Harlan Kellaway on 1/17/19.
//  Copyright © 2019 Harlan Kellaway. All rights reserved.
//

import Foundation

/// Timer.
struct Timer {
    
    /// Executes provided action after a delay of seconds.
    ///
    /// - Parameters:
    ///   - seconds: Number of seconds to delay.
    ///   - action: Action to execute after delay.
    func executeAfter(seconds: Double, action: @escaping () -> Void) {
        DispatchQueue.main.asyncAfter(deadline: .now() + seconds, execute: action)
    }
    
}
