//
//  Result.swift
//  ProlificCodeTest
//
//  Created by Harlan Kellaway on 1/17/19.
//  Copyright © 2019 Harlan Kellaway. All rights reserved.
//

import Foundation

/// Result with no value type.
typealias EmptyResult = ()

/// A type that can represent either success with a value or failure with an error.
/// Source: http://alisoftware.github.io/swift/async/error/2016/02/06/async-errors/
enum Result<T> {
    
    // MARK: - Types
    
    public typealias Value = T
    public typealias ErrorType = Error
    
    // MARK: - Cases
    
    /// Sucess with value.
    case success(Value)
    
    /// Failure with error.
    case failure(ErrorType)
    
    // MARK: - Properties
    
    /// Value associated with Result.
    /// If there is no value, this will be nil.
    ///
    /// - Returns: Value or nil.
    public var value: Value? {
        switch self {
        case .success(let value):
            return value
        case .failure:
            return nil
        }
    }
    
    /// Error associated with Result.
    /// If there is no error, this will be nil.
    ///
    /// - Returns: Value or nil.
    public var error: Error? {
        switch self {
        case .success:
            return nil
        case .failure(let error):
            return error
        }
    }
    
    /// Whether this Result represents a success.
    public var isSuccess: Bool {
        switch self {
        case .success:
            return true
        case .failure:
            return false
        }
    }
    
    /// Whether this Result represents a failure.
    public var isFailure: Bool {
        return !isSuccess
    }
    
    // MARK: - Init/Deinit
    
    /// Initializes a Result with the specified value.
    ///
    /// - parameter value: Value.
    ///
    /// - returns: Result with value.
    public init(value: Value) {
        self = .success(value)
    }
    
    /// Initializes a Result with the specified error.
    ///
    /// - parameter error: Error.
    ///
    /// - returns: Result with error.
    public init(error: ErrorType) {
        self = .failure(error)
    }
    
    /// Initializes a Result with a value or error returned from the specified expression.
    ///
    /// Note: @autoclosure automatically creates a closure around the expression passed
    /// into the fnction and enables expression-based initialization (e.g. Result(1 < 2)).
    ///
    /// - parameter throwingExpr: Expression that returns either a value or an error.
    ///
    /// - returns: Result with value or error.
    public init(_ throwingExpr: @autoclosure () throws -> Value) {
        self.init(attempt: throwingExpr)
    }
    
    /// Initializes a Result with value or error returned from the specified closure.
    ///
    /// - parameter throwingClosure: Closure that returns either a value or an error.
    ///
    /// - returns: Result with value or error.
    public init(attempt throwingClosure: () throws -> Value) {
        do {
            self = .success(try throwingClosure())
        } catch {
            self = .failure(error)
        }
    }
    
    // MARK: - Instance functions
    
    /// Returns a Result containing the outcome of performing the transform closure.
    ///
    /// - parameter transform: Transform closure.
    ///
    /// - returns: Result with value or error.
    public func map<U>(_ transform: (Value) -> U) -> Result<U> {
        switch self {
        case .success(let value):
            return .success(transform(value))
        case .failure(let error):
            return .failure(error)
        }
    }
    
    /// Returns a Result returned from the outcome of the transform closure.
    ///
    /// - parameter transform: Transform closure.
    ///
    /// - returns: Result with value or error.
    public func flatMap<U>(_ transform: (Value) -> Result<U>) -> Result<U> {
        switch self {
        case .success(let value):
            return transform(value)
        case .failure(let error):
            return .failure(error)
        }
    }
    
    /// Resolves the receiver by either returning a value or throwing an error.
    ///
    /// - throws: Error associated with the receiver.
    ///
    /// - returns: Value associated with the receiver.
    public func resolve() throws -> Value {
        switch self {
        case .success(let value):
            return value
        case .failure(let error):
            throw error
        }
    }
    
}
