//
//  Repository.swift
//  ProlificCodeTest
//
//  Created by Harlan Kellaway on 1/17/19.
//  Copyright © 2019 Harlan Kellaway. All rights reserved.
//

import Foundation

/// Data repository. Abstracts where data is accessed from, whether remote or local.
final class Repository: HomeRepository {
    
    // MARK: - Properties
    
    /// HTTP client.
    let httpClient: HTTPClient
    
    /// JSON parser.
    let jsonParser: JSONParser
    
    // MARK: - Initialization
    
    init(httpClient: HTTPClient, jsonParser: JSONParser) {
        self.httpClient = httpClient
        self.jsonParser = jsonParser
    }
    
    // MARK: - Protocol conformance
    
    // MARK: HomeRespository
    
    func getBooks(completion: @escaping (Result<[Book]>) -> Void) {
        httpClient.executeRequest(method: .GET, path: "books", parameters: nil) { [weak self] result in
            guard let `self` = self else { return }
            
            switch result {
            case .success(let data):
                completion(self.jsonParser.parseBooks(data))
            case.failure(let error):
                completion(Result(error: error))
            }
        }
    }
    
}
