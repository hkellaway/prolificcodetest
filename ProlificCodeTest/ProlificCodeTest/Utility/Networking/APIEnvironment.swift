//
//  Environment.swift
//  ProlificCodeTest
//
//  Created by Harlan Kellaway on 1/17/19.
//  Copyright © 2019 Harlan Kellaway. All rights reserved.
//

import Foundation

/// Application environments.
///
/// - production: Production.
/// - staging: Staging.
enum APIEnvironment {
    case production
    case staging
    case undefined
    
    /// Whether current Environment is a Production Environment.
    var isProduction: Bool {
        switch self {
        case .production:
            return true
        default:
            return false
        }
    }
    
    /// Whether current Environment is undefined.
    var isUndefined: Bool {
        switch self {
        case .undefined:
            return true
        default:
            return false
        }
    }
    
    /// Base URL associated with APi Environment.
    ///
    /// - Returns: Base URL for API.
    func baseURL() -> String {
        switch self {
        case .production:
            return "http://prolific-interview.herokuapp.com/5c40cf52fd740b00097ff9de"
        case .staging:
            return "http://prolific-interview.herokuapp.com/5c40cf52fd740b00097ff9de"
        case .undefined:
            return ""
        }
    }
    
}
