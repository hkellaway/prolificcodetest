//
//  HTTPClient.swift
//  ProlificCodeTest
//
//  Created by Harlan Kellaway on 1/17/19.
//  Copyright © 2019 Harlan Kellaway. All rights reserved.
//

import Foundation

/// Makes HTTP requests.
protocol HTTPClient {
    
    /// Whether High Latency is currently being simulated.
    var isSimulatingHighLatency: Bool { get }
    
    /// Executes HTTP request.
    ///
    /// - Parameters:
    ///   - method: HTTP method.
    ///   - path: Path.
    ///   - parameters: Parameters.
    ///   - completion: Completion with JSON when successful, error otherwise.
    func executeRequest(method: HTTPMethod,
                        path: String,
                        parameters: HTTPRequestParameters?,
                        completion: @escaping (Result<Data>) -> Void)
    
    /// Toggles whether High Latency is applied to reqpests.
    func toggleHighLatency()
    
}
