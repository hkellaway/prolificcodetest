//
//  HTTPMethod.swift
//  ProlificCodeTest
//
//  Created by Harlan Kellaway on 1/17/19.
//  Copyright © 2019 Harlan Kellaway. All rights reserved.
//

import Foundation

/// HTTP method.
///
/// - GET: GET.
/// - POST: POST.
/// - DELETE: DELETE.
enum HTTPMethod: CustomStringConvertible {
    case GET
    case POST
    case DELETE
    
    var description: String {
        switch self {
        case .GET:
            return "GET"
        case .POST:
            return "POST"
        case .DELETE:
            return "DELETE"
        }
    }
}
