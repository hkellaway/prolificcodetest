//
//  HTTPClientError.swift
//  ProlificCodeTest
//
//  Created by Harlan Kellaway on 1/17/19.
//  Copyright © 2019 Harlan Kellaway. All rights reserved.
//

import Foundation

enum HTTPClientError: Error {
    case requestInvalidURL(value: String)
    case responseInvalidFormat
    case responseInvalidStatusCode(value: Int)
    case responseNoData
    case responeNotJSON
    case responseError(value: Error)
}
