//
//  BasicHTTPClient.swift
//  ProlificCodeTest
//
//  Created by Harlan Kellaway on 1/17/19.
//  Copyright © 2019 Harlan Kellaway. All rights reserved.
//

import Foundation

final class BasicHTTPClient: HTTPClient {
    
    // MARK: - Properties
    
    /// Base URL.
    let baseURL: String
    
    /// URL session used to make requests.
    let urlSession: URLSession
    
    /// Timer.
    let timer: Timer
    
    /// Whether High Latency is being simulated.
    private(set) var isSimulatingHighLatency: Bool
    
    // MARK: - Initialization
    
    init(baseURL: String,
         timer: Timer = Timer(),
         isSimulatingHighLatency: Bool,
         urlSession: URLSession = URLSession.shared
        ) {
        self.baseURL = baseURL
        self.timer = timer
        self.isSimulatingHighLatency = isSimulatingHighLatency
        self.urlSession = urlSession
    }
    
    // MARK: - Instance functions
    
    func toggleHighLatency() {
        isSimulatingHighLatency = !isSimulatingHighLatency
    }
    
    func executeRequest(method: HTTPMethod,
                        path: String,
                        parameters: HTTPRequestParameters?,
                        completion: @escaping (Result<Data>) -> Void) {
        let urlString = "\(baseURL)/\(path)"
        guard let url = URL(string: urlString) else {
            completion(Result(error: HTTPClientError.requestInvalidURL(value: urlString)))
            return
        }
        
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = method.description
        
        urlSession.dataTask(with: urlRequest) { [weak self] (data, response, error) in
            guard let `self` = self else { return }
            
            let responseData = self.dataFromResponse((data, response, error))
            
            if self.isSimulatingHighLatency {
                self.timer.executeAfter(seconds: 5) {
                    DispatchQueue.main.async {
                        completion(responseData)
                    }
                }
            } else {
                DispatchQueue.main.async {
                    completion(responseData)
                }
            }
        }.resume()
    }
    
    // MARK: Private functions
    
    private func dataFromResponse(_ response: (Data?, URLResponse?, Error?)) -> Result<Data> {
        if let error = response.2 {
            return Result(error: HTTPClientError.responseError(value: error))
        }
        
        guard let httpResponse = response.1 as? HTTPURLResponse else {
            return Result(error: HTTPClientError.responseInvalidFormat)
        }
        
        guard (200...299).contains(httpResponse.statusCode) else {
            return Result(error: HTTPClientError.responseInvalidStatusCode(value: httpResponse.statusCode))
        }
        
        guard let data = response.0 else {
            return Result(error: HTTPClientError.responseNoData)
        }
        
        return Result(value: data)
    }
    
}
