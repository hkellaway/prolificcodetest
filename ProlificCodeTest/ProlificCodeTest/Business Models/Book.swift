//
//  Book.swift
//  ProlificCodeTest
//
//  Created by Harlan Kellaway on 1/17/19.
//  Copyright © 2019 Harlan Kellaway. All rights reserved.
//

import Foundation

/// Book model.
struct Book: Codable {
    
    // MARK: - Properties
    
    /// Title.
    let title: String
    
}
